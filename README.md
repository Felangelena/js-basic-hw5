## Теоретичні питання

1. Опишіть своїми словами, що таке метод об'єкту

Метод об'єкту - це функція, яка зберігається в об'єкті як її властивість.

2. Який тип даних може мати значення властивості об'єкта?

Властивості об'єкта типу string

3. Об'єкт це посилальний тип даних. Що означає це поняття?

Означає, що змінна, яка містить об'єкт, фактично не містить копії цих даних, а тільки посилання на місце в пам'яті, де ці дані зберігаються.
